package com.ipvision.sulaiman.stoppromotionalsms;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ipvision.sulaiman.stoppromotionalsms.adapter.ViewPagerAdapter;
import com.ipvision.sulaiman.stoppromotionalsms.fragments.BlockContactListViewFragmnet;
import com.ipvision.sulaiman.stoppromotionalsms.fragments.ContactListViewFragment;
import com.ipvision.sulaiman.stoppromotionalsms.fragments.SMSListViewFragment;
import com.ipvision.sulaiman.stoppromotionalsms.utility.Logger;


public class HomeActivity extends AppCompatActivity {

    public static final String TAG = "HomeActivity";

    private ViewPager viewPager;
    private ViewPagerAdapter pagerAdapter;
    private TabLayout mTabLayout;

    private SMSListViewFragment smsListViewFragment;

    public static final int TAB_POSITION_MESSAGES = 0;
    public static final int TAB_POSITION_CONTACTS = 1;
    public static final int TAB_POSITION_BLOCKED_CONTACTS = 2;
    public static final String TABS_COUNT = "tabscount";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.debugLog(TAG, "onCreate");
        setContentView(R.layout.activity_home);

        setupTabLayout(savedInstanceState);

    }

    private void setupTabLayout(Bundle savedInstance) {
        Logger.debugLog(TAG,"setupTabLayout");
        viewPager = (ViewPager) findViewById(R.id.home_view_pager);
        pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        if (savedInstance == null) {
            smsListViewFragment = new SMSListViewFragment();

            pagerAdapter.addFragment(smsListViewFragment, "Messages");
            pagerAdapter.addFragment(new ContactListViewFragment(), "Contact");
            pagerAdapter.addFragment(new BlockContactListViewFragmnet(), "Blocked Contacts");

        } else {
            Integer count = savedInstance.getInt(TABS_COUNT);
            String[] titles = savedInstance.getStringArray("titles");
            for (int i = 0; i < count; i++) {
                pagerAdapter.addFragment(getFragment(i, savedInstance), titles[i]);
            }
        }

        viewPager.setAdapter(pagerAdapter);
        viewPager.setOffscreenPageLimit(3);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mTabLayout = (TabLayout) findViewById(R.id.tablayout);
        mTabLayout.setupWithViewPager(viewPager);

    }

    private Fragment getFragment(int position, Bundle savedInstanceState) {
        Logger.errorLog(TAG, "getFragment");
        return savedInstanceState == null ? pagerAdapter.getItem(position) : getSupportFragmentManager().findFragmentByTag(getFragmentTag(position));
    }

    private String getFragmentTag(int position) {
        Logger.debugLog(TAG,"getFragmentTag");
        String tag = "android:switcher:" + R.id.home_view_pager + ":" + position;
        Logger.errorLog(TAG, "getFragmentTag " + tag);
        return tag;
    }
}
