package com.ipvision.sulaiman.stoppromotionalsms.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;


import com.ipvision.sulaiman.stoppromotionalsms.R;
import com.ipvision.sulaiman.stoppromotionalsms.adapter.SMSAdapter;
import com.ipvision.sulaiman.stoppromotionalsms.model.SMS;
import com.ipvision.sulaiman.stoppromotionalsms.utility.Logger;
import com.ipvision.sulaiman.stoppromotionalsms.utility.PermissionManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sulaiman on 6/5/2017.
 */

public class SMSListViewFragment extends Fragment {
    private static final String TAG = "SMSListViewFragment";

    private SMSAdapter mSMSAdapter;
    private List<SMS> mSMSList = new ArrayList<>();
    private RecyclerView mRecyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sms_recycler_view, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);

        mSMSAdapter = new SMSAdapter(mSMSList);


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);

        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mRecyclerView.setAdapter(mSMSAdapter);

        if (PermissionManager.checkPermission(getActivity(), Manifest.permission.READ_SMS)) {
            try{
                prepareSMStoShow();
            } catch (Exception e){
                Logger.errorLog(TAG,"onCreateView prepareSMStoShow method call "+e.toString());
            }

        } else {
            Toast.makeText(getContext(), "App is exiting, please give permission", Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_SMS}, PermissionManager.REQUEST_CODE_SMS_READ);

        }

        return view;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Logger.debugLog(TAG, "requestCode: " + requestCode);
        switch (requestCode) {
            case PermissionManager.REQUEST_CODE_SMS_READ:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    try{
                        prepareSMStoShow();
                    } catch (Exception e){
                        Logger.errorLog(TAG,"onRequestPermissionsResult prepareSMStoShow method call "+e.toString());
                    }
                else
                    getActivity().finish();
                break;
        }

    }

    private void prepareSMStoShow() {
        Logger.debugLog(TAG, "prepareSMStoShow");

        Uri uri = Uri.parse("content://sms/inbox");
        Cursor c = getActivity().getContentResolver().query(uri, null, null, null, null);
        getActivity().startManagingCursor(c);

        // Read the sms data and store it in the list
        if (c.moveToFirst()) {
            for (int i = 0; i < c.getCount(); i++) {
                SMS sms = new SMS();
                sms.setSmsContentText(c.getString(c.getColumnIndexOrThrow("body")).toString());
                sms.setTitle(c.getString(c.getColumnIndexOrThrow("address")).toString());
                mSMSList.add(sms);

                c.moveToNext();
            }
        }
        c.close();

        mSMSAdapter.notifyDataSetChanged();
    }
}
