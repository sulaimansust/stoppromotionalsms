package com.ipvision.sulaiman.stoppromotionalsms.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.ipvision.sulaiman.stoppromotionalsms.R;
import com.ipvision.sulaiman.stoppromotionalsms.model.SMS;

import java.util.List;

/**
 * Created by sulaimansust on 5/17/17.
 */


public class SMSAdapter extends RecyclerView.Adapter<SMSAdapter.SMSViewHolder> {

    private List<SMS> mSMSList;


    public class SMSViewHolder extends RecyclerView.ViewHolder {

        private ImageView smsProfileImage;
        private TextView smsTitle, smsContentText;


        public SMSViewHolder(View itemView) {
            super(itemView);
            this.smsProfileImage = (ImageView) itemView.findViewById(R.id.sms_circle_image);
            this.smsTitle = (TextView) itemView.findViewById(R.id.sms_title);
            this.smsContentText = (TextView) itemView.findViewById(R.id.sms_content_text);
        }

    }

    public SMSAdapter(List<SMS> SMSList) {
        mSMSList = SMSList;
    }

    @Override
    public SMSViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sms_row, parent, false);

        return new SMSViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SMSViewHolder holder, int position) {
        SMS sms = mSMSList.get(position);
        holder.smsTitle.setText(sms.getTitle());
        holder.smsContentText.setText(sms.getSmsContentText());

    }

    @Override
    public int getItemCount() {
        return mSMSList.size();
    }
}
