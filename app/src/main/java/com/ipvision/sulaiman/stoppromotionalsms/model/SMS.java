package com.ipvision.sulaiman.stoppromotionalsms.model;

/**
 * Created by sulaimansust on 5/17/17.
 */
public class SMS {
    private String title, smsContentText;

    public SMS(String title, String smsContentText) {
        this.title = title;
        this.smsContentText = smsContentText;
    }

    public SMS() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSmsContentText() {
        return smsContentText;
    }

    public void setSmsContentText(String smsContentText) {
        this.smsContentText = smsContentText;
    }
}