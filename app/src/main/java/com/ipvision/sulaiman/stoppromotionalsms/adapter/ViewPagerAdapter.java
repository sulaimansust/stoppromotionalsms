package com.ipvision.sulaiman.stoppromotionalsms.adapter;

import android.support.annotation.BoolRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.ipvision.sulaiman.stoppromotionalsms.fragments.BlockContactListViewFragmnet;
import com.ipvision.sulaiman.stoppromotionalsms.fragments.ContactListViewFragment;
import com.ipvision.sulaiman.stoppromotionalsms.fragments.SMSListViewFragment;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by sulaiman on 6/5/2017.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragmentList;
    private List<String> fragmentTitleList;

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
        fragmentList = new ArrayList<>();
        fragmentTitleList = new ArrayList<>();
    }

    public void addFragment(Fragment fragment, String title) {
        fragmentList.add(fragment);
        fragmentTitleList.add(title);
    }


    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentTitleList.get(position);
    }

    public List<String> getFragmentTitleList() {
        return fragmentTitleList;
    }
}
